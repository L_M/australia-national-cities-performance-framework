# Australia National Cities Performance Framework

Data sourced from https://www.bitre.gov.au/national-cities-performance-framework#all_cities

## Showcasing the dataset in Tableau

> **[What Australian City Should I Live in?](https://public.tableau.com/app/profile/lm2022/viz/WhereShouldILiveInteractiveAustralianCityChooser/Chooser-WhereShouldYouLive)** created in Tableau Public.

### Data cleaning methodology:

The following attributes were standardised (to fit range 0-1):

```
access_to_open_space
air_quality
volunteering
active_transport
broadband_connections
arts_infrastructure
crisis_support
building_approvals
dwelling_energy_eff
education
education_infrastructure
employment_growth
economic.output
health_infrastructure
household_income
jobs_accessible_number
knowledge_industries
life_expectancy
new_business
walkability
participation
public_transport_access
safety
sports_infrastructure
```

The following attributes were standardised (to fit range 0-1), then subtracted from 1 (lower is a better measure):

```
X.greenhouse_gas_em_per_cap
X.homelessness
X.house_price_to_income_ratio
X.housing_construction_costs
X.housing_price
X.indigenous_unemployment
X.mortgage_stress
X.obesity
X.rent_stress
X.road_safety
X.unemployment
X.unit_price
```

The final dataset (*National-Cities-Performance-Framework-loc_rows.csv*) used is in city (rows) vs attribute (columns) tabular form.
